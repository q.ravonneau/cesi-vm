# --------------- DÉBUT COUCHE OS -------------------
FROM debian:stable-slim
# --------------- FIN COUCHE OS ---------------------
# MÉTADONNÉES DE L'IMAGE
LABEL version="1.0" maintainer="victor HUGO <vhugo@labas.com>"
# VARIABLES TEMPORAIRES
ARG APT_FLAGS="--quiet --yes"
ARG DOCUMENTROOT="/var/www/html"
# --------------- DÉBUT COUCHE APACHE ---------------
RUN apt-get update --yes && \
apt-get install ${APT_FLAGS} apache2
# --------------- FIN COUCHE APACHE -----------------
# --------------- DÉBUT COUCHE MYSQL ----------------
RUN apt-get install ${APT_FLAGS} mariadb-server
COPY db/articles.sql /
# --------------- FIN COUCHE MYSQL ------------------
# --------------- DÉBUT COUCHE PHP ------------------
RUN apt-get install ${APT_FLAGS} \
php-mysql \
php && \
rm -f ${DOCUMENTROOT}/index.html && \
apt-get autoclean -y
COPY app ${DOCUMENTROOT}
# --------------- FIN COUCHE PHP --------------------
# OUVERTURE DU PORT HTTP
EXPOSE 80
# RÉPERTOIRE DE TRAVAIL
WORKDIR ${DOCUMENTROOT}
# DÉMARRAGE DES SERVICES LORS DE L'EXÉCUTION DE L'IMAGE
ENTRYPOINT service mariadb start && mariadb < /articles.sql && apache2ctl -D FOREGROUND